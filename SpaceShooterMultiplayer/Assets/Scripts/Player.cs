using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;

public class Player : NetworkBehaviour
{
    [Header("UI")]
    public GameObject playerNameCanvasPrefab;
    public GameObject nameCanvas;
    public Text nameText;
    public Vector3 nameCanvasOffset;
    public Text lifePointsText;


    [Header("Balancing")]
    public float moveForce = 450;
    public float rotationSpeed = 450;
    public GameObject bulletPrefab;
    public Transform bulletSpawnPoint;

    /* 
     * Anlegen der NetworkVariables
     */
    [Header("Networkvariables (Owner)")]
    private NetworkVariable<int> lifepoints = new NetworkVariable<int>(writePerm: NetworkVariableWritePermission.Owner);
    //private NetworkVariable<int> score = new NetworkVariable<int>(writePerm: NetworkVariableWritePermission.Owner);

    private Camera camera;
    private Rigidbody rigidbody;


    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        rigidbody = GetComponent<Rigidbody>();
        nameCanvas = Instantiate(playerNameCanvasPrefab);
        nameText = nameCanvas.transform.GetChild(0).GetComponent<Text>();
        if (IsOwner)
        {
            /* 
             * Besser wäre es, wenn man statt "GameObject.Find" eine Refrenz über einen GameManager verwenden würde.
             * Hier dient das Vorgehen nur als Demonstration. GameObject.Find nicht im produktiven Spiel einsetzen !
             */
            lifePointsText = GameObject.Find("lifePointsText").GetComponent<Text>();
            /* 
             * Hier wird ein callback registriert, der aufgerufen wird, wenn sich der Wert ändert.
             * Der Callback erwartet zwei Parameter <T> oldValue und <T> newValue.
             * */
            lifepoints.OnValueChanged += RefreshLifepointsUI;
            /* lifepoints werden initial gesetzt */
            lifepoints.Value = 3;
            

            score.Value = 0;
        }
    }

    private void FixedUpdate()
    {
        
        RotateToMouse();
    }

    // Update is called once per frame
    void Update()
    {
        if (IsOwner) { 
            if (Input.GetKey(KeyCode.W))
            {
                rigidbody.AddForce(transform.forward * Time.deltaTime * moveForce);
            }
            if (Input.GetMouseButtonDown(0))
            {
                SpawnBulletServerRPC(bulletSpawnPoint.position, transform.rotation, OwnerClientId);
            }

            moveNameCanvas();
            moveCamera();
        }
    }

    void RotateToMouse()
    {
        Ray ray = camera.ScreenPointToRay(Input.mousePosition);
        if (true)
        {
            Vector3 hitPoint = ray.GetPoint(100);
            hitPoint.y = 1;
            Vector3 direction = hitPoint - transform.position;
            direction.y = 0;
            Quaternion rotation = Quaternion.LookRotation(direction);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
        }
    }

    void moveCamera()
    {
        camera.gameObject.transform.position = transform.position + Vector3.up;
    }

    void moveNameCanvas()
    {
        nameCanvas.transform.position = transform.position + Vector3.forward;
    }


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision:" + collision.gameObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        Bullet bullet = other.GetComponent<Bullet>();
        //Debug.Log("Trigger:" + other.name + " playerid: " + OwnerClientId + " collidee: " + bullet.playerId);
        if (OwnerClientId != bullet.playerId)
        {
            Debug.Log("HIT");
            bullet.DestroyBullet();
            lifepoints.Value--;
        }
    }

    /* 
     * NetworkVariable callback für lifepoints
     */
    private void RefreshLifepointsUI(int previous, int current)
    {
        lifePointsText.text = current.ToString();
    }


    [ServerRpc]
    public void SpawnBulletServerRPC(Vector3 position, Quaternion rotation, ulong playerId)
    {
        GameObject newBulletGameObject = Instantiate(bulletPrefab, position, rotation);
        newBulletGameObject.GetComponent<NetworkObject>().Spawn();
        newBulletGameObject.GetComponent<Bullet>().playerId = playerId;
    }
}
