using UnityEngine;
using Unity.Netcode;

public class Bullet : NetworkBehaviour
{
    public float lifetime = 2;
    public float moveForce = 4;
    public ulong playerId;


    private Rigidbody rigidbody;
    private float destroyTime;

    #region lifecycle
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        destroyTime = Time.time + lifetime;
        Invoke("DestroyBullet", lifetime);
    }

    void Update()
    {
        if (Time.time >= destroyTime) {
            DestroyBullet();
        }
        rigidbody.velocity = transform.forward * moveForce;
    }
    #endregion



    public void DestroyBullet()
    {
        if (IsServer)
        {
            GetComponent<NetworkObject>().Despawn();
        }
    }
}
